﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : An item for the ComboBox that contains the MagazinType
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{
    class ComboItem
    {
        public MagazineType Type { get; set; }

        public override string ToString()
        {
            return Type.ToString();
        }
    }
}
