﻿/**
 * @author  : Metin Cem Demirdaş
 * @number  : 152120181015
 * @mail    : cem.dmrds@hotmail.com
 * @date    : 04/06/2021
 * @brief   : class for product type and the amount of products.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{   
    /// <summary>
    /// class to determine which/how many products to be purchased
    /// </summary>
    class ItemToPurchase
    {
        private Product product;
        private int quantity;

        public Product Product { get { return this.product; } }
        public int Quantity { get { return this.quantity; } set { this.quantity = value; } }
        /// <summary>
        /// the constructor of ItemToPurchase
        /// </summary>
        /// <param name="product">products.(book, magazine and music cd)</param>
        /// <param name="quantity">the amount of the products</param>
        public ItemToPurchase(Product product, int quantity)
        {
            this.product = product;
            this.quantity = quantity;
        }
    }
}
