﻿/**
 * @author  : Metin Cem Demirdaş
 * @number  : 152120181015
 * @mail    : cem.dmrds@hotmail.com
 * @date    : 04/06/2021
 * @brief   : abstract product class for all of the products.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{
    /// <summary>
    /// This class holds the common attributes of all the products.
    /// </summary>
    abstract class Product
    {
        protected ProductType productType;
        protected string id;
        protected string name;
        protected float price;
        protected byte[] picture;

        public string ID { get { return this.id; } set { this.id = value; } } 
        public string Name { get { return this.name; } set { this.name = value; } } 
        public float Price { get { return this.price; } set { this.price = value; } }
        public byte[] Picture { get { return this.picture; } set { this.picture = value; } }
        public ProductType ProductType { get { return this.productType; } } 

        /// <summary>
        /// since all of the products' info will be printed, we can override the printing function from here
        /// </summary>
        public abstract void printProperties();
    }
}
