﻿/**
*  @author  : Caner Sülüşoğlu
*  @number  : 152120181022
*  @mail    : cscaner26@gmail.com
*  @date    : 04.06.2021
*  @brief   : Shopping cart operations.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore
{
    enum PaymentType
    {
        Cash,
        CreditCard
    }

    /// <summary>
    /// Class that access to use shopping cart items.
    /// </summary>
    class ShoppingCart
    {
        private string customerId;
        private List<ItemToPurchase> itemToPurchases;
        private PaymentType paymentType;

        public string CustomerID { get { return this.customerId; } }
        public PaymentType PaymentType { get { return this.paymentType; } set { this.paymentType = value; } }
        public List<ItemToPurchase> ItemToPurchases { get { return this.itemToPurchases; } }

        /// <summary>
        /// Constructor for ShoppingCart class.
        /// </summary>
        /// <param name="customerId"></param>
        public ShoppingCart(string customerId)
        {
            this.customerId = customerId;
            this.itemToPurchases = new List<ItemToPurchase>();
        }

        /// <summary>
        /// Method that gets total price.
        /// </summary>
        /// <returns>Total price in shopping cart.</returns>
        public float getTotalPrice()
        {
            float total = 0f;
            foreach (ItemToPurchase item in ItemToPurchases)
            {
                total += item.Product.Price * item.Quantity;
            }
            return total;
        }

        /// <summary>
        /// Method that adds product to shopping cart.
        /// </summary>
        /// <param name="purchaseItem">Item that adds to shopping cart.</param>
        public void addProduct(ItemToPurchase purchaseItem)
        {
            ItemToPurchase item = itemToPurchases.Find(x => x.Product.ProductType == purchaseItem.Product.ProductType && x.Product.ID == purchaseItem.Product.ID);
            if (item != null)
            {
                item.Quantity += purchaseItem.Quantity;
            }
            else
            {
                itemToPurchases.Add(purchaseItem);
            }
        }

        /// <summary>
        /// Method that removes product to shopping cart.
        /// </summary>
        /// <param name="purchaseItem">Item that removes to shopping cart.</param>
        public void removeProduct(ItemToPurchase purchaseItem)
        {
            ItemToPurchase item = itemToPurchases.Find(x => x.Product.ProductType == purchaseItem.Product.ProductType && x.Product.ID == purchaseItem.Product.ID);
            if (item != null)
            {
                itemToPurchases.Remove(item);
            }
        }

        /// <summary>
        /// Confirm and buy items in shopping carts.
        /// </summary>
        public void placeOrder()
        {
            bool result = Database.getInstance().confirmShoppingCart();
            if (result)
            {
                sendInvoidcebyEmail();
                sendInvoicebySMS();
                this.cancelOrder();
            }
        }

        /// <summary>
        /// Removes all items in shopping carts.
        /// </summary>
        public void cancelOrder()
        {
            if (itemToPurchases.Count > 0)
            {
                itemToPurchases.Clear();
            }
        }

        /// <summary>
        /// Sends Email to customer for information about bill.
        /// </summary>
        private void sendInvoidcebyEmail()
        {
            
        }

        /// <summary>
        /// Sends SMS to customer for information about bill.
        /// </summary>
        private void sendInvoicebySMS()
        {
            
        }

        /// <summary>
        /// Prints information about all products in shopping cart.
        /// </summary>
        public void printProducts()
        {
            foreach (ItemToPurchase item in itemToPurchases)
            {
                Console.WriteLine("*********************");
                item.Product.printProperties();
                Console.WriteLine("*********************");
            }
        }
    }
}
