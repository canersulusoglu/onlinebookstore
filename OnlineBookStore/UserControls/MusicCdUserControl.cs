﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Listing music cds in database.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that lists the music cds in the database.
    /// </summary>
    public partial class MusicCdUserControl : UserControl
    {
        private IList<Product> MusicCds;
        private ShoppingCart shoppingCart = Database.getInstance().getLoggedCustomer().ShoppingCart;

        /// <summary>
        /// Constructor for MusicCdUserControl class.
        /// </summary>
        public MusicCdUserControl()
        {
            MusicCds = Database.getInstance().getMusicCds();
            InitializeComponent();
        }

        /// <summary>
        /// Method that fills the list with music cds from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MusicCdUserControl_Load(object sender, EventArgs e)
        {
            foreach (var cd in MusicCds)
            {
                listViewMusicCds.Items.Add(new ListViewItem(new string[] { cd.Name, ((MusicCd)cd).Type.ToString(), ((MusicCd)cd).Singer, cd.Price.ToString() }));
            }
        }

        /// <summary>
        /// Method that shows the picture of the selected item from the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listViewMusicCds_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewMusicCds.SelectedItems.Count > 0)
            {
                var selectedCd = MusicCds.FirstOrDefault(c => c.Name == listViewMusicCds.SelectedItems[0].SubItems[0].Text);
                MemoryStream mStream = new MemoryStream();
                byte[] pData = selectedCd.Picture;
                mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                Bitmap bm = new Bitmap(mStream, false);
                mStream.Dispose();
                pictureBox2.Image = bm;

                labelMusicCdName.Name = selectedCd.Name;
            }
        }

        /// <summary>
        /// Method that adds the music cd tho the shopping cart by ItemToPurchase class.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (listViewMusicCds.SelectedItems.Count > 0)
            {
                if (textBoxQuantity.Text != "")
                {
                    if (int.Parse(textBoxQuantity.Text) > 0)
                    {
                        if (listViewMusicCds.SelectedItems.Count > 0)
                        {

                            var selectedMusicCd = MusicCds.FirstOrDefault(b => b.Name == listViewMusicCds.SelectedItems[0].SubItems[0].Text);

                            ItemToPurchase itemToPurchase = new ItemToPurchase(selectedMusicCd, int.Parse(textBoxQuantity.Text));
                            shoppingCart.addProduct(itemToPurchase);
                            MessageBox.Show(int.Parse(textBoxQuantity.Text) + " " + selectedMusicCd.Name + "is added successfully to the shopping cart");
                        }
                        else
                            MessageBox.Show("Please select an item from the list");
                    }
                    else
                    {
                        MessageBox.Show("Quantity area can't be smaller than 1");
                    }
                }
                else
                    MessageBox.Show("Please enter a number to the quantity area");
            }
            else
                MessageBox.Show("Please select an item from the list");
        }
    }
}
