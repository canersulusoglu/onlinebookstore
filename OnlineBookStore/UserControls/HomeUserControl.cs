﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Home page.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that have short paths to the product lists..
    /// </summary>
    public partial class HomeUserControl : UserControl
    {
        /// <summary>
        /// Constructor for HomeUserControl class.
        /// </summary>
        public HomeUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that opens the BooksUserControl by clicking the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelBooksHome_Click(object sender, EventArgs e)
        {
            BooksUserControl booksUserControl = new BooksUserControl();
            this.Controls.Clear();
            this.Controls.Add(booksUserControl);
        }

        /// <summary>
        /// Method that opens the MagazinesUserControl by clicking the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelMagazinesHome_Click(object sender, EventArgs e)
        {
            MagazinesUserControl magazinesUserControl = new MagazinesUserControl();
            this.Controls.Clear();
            this.Controls.Add(magazinesUserControl);
        }

        /// <summary>
        /// Method that opens the MusicCdUserControl by clicking the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelMusicCdsHome_Click(object sender, EventArgs e)
        {
            MusicCdUserControl musicCdUserControl = new MusicCdUserControl();
            this.Controls.Clear();
            this.Controls.Add(musicCdUserControl);
        }
    }
}
