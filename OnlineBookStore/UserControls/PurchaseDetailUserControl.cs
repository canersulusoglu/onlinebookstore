﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Purchase details
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that lists the details of a purchase made by the customer.
    /// </summary>
    public partial class PurchaseDetailUserControl : UserControl
    {
        private BillDetail billDetail;
        private IList<ItemToPurchase> billItems;

        /// <summary>
        /// Constructor for PurchaseDetailUserControl class.
        /// </summary>
        /// /// <param name="billId">Customer username.</param>
        public PurchaseDetailUserControl(string billId)
        {
            InitializeComponent();
            billDetail = Database.getInstance().getBillWithDetail(int.Parse(billId));
            billItems = billDetail.BillProducts;
        }

        /// <summary>
        /// Method that fills the list with the product details of a purchase of the customer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurchaseDetailUserControl_Load(object sender, EventArgs e)
        {
            foreach (var item in billItems)
            {
                MemoryStream mStream = new MemoryStream();
                byte[] pData = item.Product.Picture;
                mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                Bitmap bm = new Bitmap(mStream, false);
                mStream.Dispose();

                flowLayoutPanelCart.Controls.Add(new BillDetailItemUserControl
                {
                    Name = item.Product.Name,
                    Category = item.Product.ProductType.ToString(),
                    Picture = bm,
                    Price = item.Product.Price.ToString(),
                    Total = (item.Product.Price * item.Quantity).ToString()
                });
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PurchasesUserControl purchasesUserControl = new PurchasesUserControl();
            this.Controls.Clear();
            this.Controls.Add(purchasesUserControl);
        }
    }
}
