﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Shopping cart operations
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that lists the products in the shopping cart.
    /// </summary>
    public partial class ShoppingCartUserControl : UserControl
    {
        private ShoppingCart shoppingCart;
        private IList<ItemToPurchase> items;
        private Timer timer1;


        /// <summary>
        /// Constructor for ShoppingCartUserControl class.
        /// </summary>
        public ShoppingCartUserControl()
        {
            InitializeComponent();
            shoppingCart = Database.getInstance().getLoggedCustomer().ShoppingCart;
            items = shoppingCart.ItemToPurchases;
            InitTimer();
        }

        /// <summary>
        /// Executing every second to calculate the total cost.
        /// </summary>
        public void InitTimer()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 1000;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label5.Text = shoppingCart.getTotalPrice().ToString();
        }

        /// <summary>
        /// Method that fills the list with the shoppingCartItem to show the products in the shopping cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShoppingCartUserControl_Load(object sender, EventArgs e)
        {
            if (items != null)
            {
                foreach (var item in items)
                {
                    MemoryStream mStream = new MemoryStream();
                    byte[] pData = item.Product.Picture;
                    mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                    Bitmap bm = new Bitmap(mStream, false);
                    mStream.Dispose();
                    flowLayoutPanelCart.Controls.Add(new ShoppingCartItemUserControl
                    {
                        Id = item.Product.ID,
                        Quantity = item.Quantity,
                        Name = item.Product.Name,
                        Category = item.Product.ProductType.ToString(),
                        Picture = bm,
                        Price = item.Product.Price.ToString(),
                        Total = (item.Product.Price * item.Quantity).ToString()
                    });

                }
                label5.Text = shoppingCart.getTotalPrice().ToString();
            }


        }

        /// <summary>
        /// Method that opens the PaymentUserControl to take the payment method from the customer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOrder_Click(object sender, EventArgs e)
        {


            if (shoppingCart.ItemToPurchases.Count > 0)
            {

                PaymentUserControl paymentUserControl = new PaymentUserControl();
                this.Controls.Clear();
                this.Controls.Add(paymentUserControl);

            }
            else
            {
                MessageBox.Show("Shopping cart is empty!");
            }

        }
    }
}
