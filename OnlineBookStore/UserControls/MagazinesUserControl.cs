﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Listing magazines in database.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that lists the magazines in the database.
    /// </summary>
    public partial class MagazinesUserControl : UserControl
    {
        private IList<Product> Magazines;
        private ShoppingCart shoppingCart = Database.getInstance().getLoggedCustomer().ShoppingCart;

        /// <summary>
        /// Constructor for MagazinesUserControl class.
        /// </summary>
        public MagazinesUserControl()
        {
            InitializeComponent();
            Magazines = Database.getInstance().getMagazines();
        }

        /// <summary>
        /// Method that fills the list with magazines from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MagazinesUserControl_Load(object sender, EventArgs e)
        {
            foreach (var magazine in Magazines)
            {
                listViewMagazines.Items.Add(new ListViewItem(new string[] { magazine.Name, ((Magazine)magazine).Type.ToString(), ((Magazine)magazine).Issue, magazine.Price.ToString() }));
            }
        }

        /// <summary>
        /// Method that shows the picture of the selected item from the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listViewMagazines_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewMagazines.SelectedItems.Count > 0)
            {
                var selectedMagazine = Magazines.FirstOrDefault(m => m.Name == listViewMagazines.SelectedItems[0].SubItems[0].Text);
                MemoryStream mStream = new MemoryStream();
                byte[] pData = selectedMagazine.Picture;
                mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                Bitmap bm = new Bitmap(mStream, false);
                mStream.Dispose();
                pictureBox2.Image = bm;

                labelMagazineName.Text = selectedMagazine.Name;
            }
        }

        /// <summary>
        /// Method that adds the magazine tho the shopping cart by ItemToPurchase class.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {


            if (listViewMagazines.SelectedItems.Count > 0)
            {
                if (textBoxQuantity.Text != "")
                {
                    if (int.Parse(textBoxQuantity.Text) > 0)
                    {
                        if (listViewMagazines.SelectedItems.Count > 0)
                        {
                            var selectedMagazine = Magazines.FirstOrDefault(m => m.Name == listViewMagazines.SelectedItems[0].SubItems[0].Text);

                            ItemToPurchase itemToPurchase = new ItemToPurchase(selectedMagazine, int.Parse(textBoxQuantity.Text));
                            shoppingCart.addProduct(itemToPurchase);
                            MessageBox.Show(int.Parse(textBoxQuantity.Text) + " " + selectedMagazine.Name + "is added successfully to the shopping cart");
                        }
                        else
                            MessageBox.Show("Please select an item from the list");
                    }
                    else
                    {
                        MessageBox.Show("Quantity area can't be smaller than 1");
                    }
                }
                else
                    MessageBox.Show("Please enter a number to the quantity area");


            }
            else
                MessageBox.Show("Please select an item from the list");




        }
    }
}
