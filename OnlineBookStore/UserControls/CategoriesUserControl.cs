﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Categories to list products.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that allows to choose the category to see the list of the products in the database.
    /// </summary>
    public partial class CategoriesUserControl : UserControl
    {
        /// <summary>
        /// Constructor for CategoriesUserControl class.
        /// </summary>
        public CategoriesUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that opens the BooksUserControl by clicking the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelBooks_Click(object sender, EventArgs e)
        {
            BooksUserControl booksUserControl = new BooksUserControl();
            this.Controls.Clear();
            this.Controls.Add(booksUserControl);
        }


        /// <summary>
        /// Method that opens the MagazinesUserControl by clicking the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelMagazines_Click(object sender, EventArgs e)
        {
            MagazinesUserControl magazinesUserControl = new MagazinesUserControl();
            this.Controls.Clear();
            this.Controls.Add(magazinesUserControl);
        }


        /// <summary>
        /// Method that opens the MusicCdUserControl by clicking the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelMusic_Click(object sender, EventArgs e)
        {
            MusicCdUserControl musicCdUserControl = new MusicCdUserControl();
            this.Controls.Clear();
            this.Controls.Add(musicCdUserControl);
        }
    }
}
