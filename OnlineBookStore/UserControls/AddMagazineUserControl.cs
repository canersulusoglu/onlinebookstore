﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Add magazine to database.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that adds magazine to the database.
    /// </summary>
    public partial class AddMagazineUserControl : UserControl
    {
        /// <summary>
        /// Constructor for AddMagazineUserControl class.
        /// </summary>
        public AddMagazineUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that fills the combobox with comboItems that contains MagazineType.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddMagazineUserControl_Load(object sender, EventArgs e)
        {
            comboMagazineType.DataSource = new ComboItem[] {
                new ComboItem{ Type = MagazineType.Actual },
                new ComboItem{ Type = MagazineType.News },
                new ComboItem{ Type = MagazineType.Sport },
                new ComboItem{ Type = MagazineType.Computer },
                new ComboItem{ Type = MagazineType.Technology }
            };
        }


        /// <summary>
        /// Method that adds magazine to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddMagazine_Click(object sender, EventArgs e)
        {
            if (textBoxIssue.Text == "" || textBoxName.Text == "" || textBoxPrice.Text == "" || comboMagazineType.SelectedItem.ToString() == "" || pictureBox1.Image == null)
            {
                MessageBox.Show("Please fill all areas");
            }
            else
            {
                ComboItem selectedType = (ComboItem)comboMagazineType.SelectedItem;
                MemoryStream ms = new MemoryStream();
                pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                Magazine magazine = (Magazine)ProductFactory.getInstance().ProduceProduct(ProductType.MAGAZINE);
                magazine.Issue = textBoxIssue.Text;
                magazine.Name = textBoxName.Text;
                magazine.Price = float.Parse(textBoxPrice.Text);
                magazine.Type = selectedType.Type;
                magazine.Picture = ms.ToArray();

                Database.getInstance().addMagazine(magazine);
                MessageBox.Show("Magazine successfully added to the database");
                MagazinesUserControl magazinesUserControl = new MagazinesUserControl();
                this.Controls.Clear();
                this.Controls.Add(magazinesUserControl);
            }


        }

        /// <summary>
        /// Method that adds a picture to the pictureBox to use it while saving to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChoosePicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpeg;*.bmp;*.png;*.jpg)|*.jpeg;*.bmp;*.png;*.jpg";
            if (open.ShowDialog() == DialogResult.OK)
            {
                lblFilePath.Text = open.FileName;
            }

            string image = lblFilePath.Text;
            Bitmap bmp = new Bitmap(image);
            FileStream fs = new FileStream(image, FileMode.Open, FileAccess.Read);
            byte[] bimage = new byte[fs.Length];
            fs.Read(bimage, 0, Convert.ToInt32(fs.Length));
            fs.Close();

            MemoryStream mStream = new MemoryStream();
            mStream.Write(bimage, 0, Convert.ToInt32(bimage.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            pictureBox1.Image = bm;
            pictureBox1.Visible = true;
        }
    }
}
