﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Purchase listing operations.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that lists the purchases made by the customer.
    /// </summary>
    public partial class PurchasesUserControl : UserControl
    {
        private IList<Bill> Bills;

        /// <summary>
        /// Constructor for PurchasesUserControl class.
        /// </summary>
        public PurchasesUserControl()
        {
            InitializeComponent();
            Bills = Database.getInstance().getBills(int.Parse(Database.getInstance().getLoggedCustomer().ID));
        }

        /// <summary>
        /// Method that opens the PurchaseDetailUserControl to show the details of a purchase of the customer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDetails_Click(object sender, EventArgs e)
        {
            if (listViewPurchases.SelectedItems.Count > 0)
            {
                PurchaseDetailUserControl purchaseDetailUserControl = new PurchaseDetailUserControl(listViewPurchases.SelectedItems[0].Text);
                this.Controls.Clear();
                this.Controls.Add(purchaseDetailUserControl);
            }
            else
                MessageBox.Show("Please select an item from the list");
        }

        /// <summary>
        /// Method that fills the list with the purchases of the customer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurchasesUserControl_Load(object sender, EventArgs e)
        {
            foreach (var bill in Bills)
            {
                listViewPurchases.Items.Add(new ListViewItem(new string[] { bill.Id, bill.Date.ToString(), bill.TotalPrice.ToString() }));
            }
        }
    }
}
