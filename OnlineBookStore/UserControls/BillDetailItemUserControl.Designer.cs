﻿
namespace OnlineBookStore.UserControls
{
    partial class BillDetailItemUserControl
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxCart = new System.Windows.Forms.PictureBox();
            this.lblTotalCart = new System.Windows.Forms.Label();
            this.lblPriceCart = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCart)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCart
            // 
            this.pictureBoxCart.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxCart.Name = "pictureBoxCart";
            this.pictureBoxCart.Size = new System.Drawing.Size(134, 134);
            this.pictureBoxCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCart.TabIndex = 12;
            this.pictureBoxCart.TabStop = false;
            // 
            // lblTotalCart
            // 
            this.lblTotalCart.AutoSize = true;
            this.lblTotalCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTotalCart.Location = new System.Drawing.Point(679, 60);
            this.lblTotalCart.Name = "lblTotalCart";
            this.lblTotalCart.Size = new System.Drawing.Size(51, 24);
            this.lblTotalCart.TabIndex = 15;
            this.lblTotalCart.Text = "Total";
            this.lblTotalCart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPriceCart
            // 
            this.lblPriceCart.AutoSize = true;
            this.lblPriceCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPriceCart.Location = new System.Drawing.Point(519, 60);
            this.lblPriceCart.Name = "lblPriceCart";
            this.lblPriceCart.Size = new System.Drawing.Size(53, 24);
            this.lblPriceCart.TabIndex = 16;
            this.lblPriceCart.Text = "Price";
            this.lblPriceCart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(68)))), ((int)(((byte)(32)))));
            this.lblCategory.Location = new System.Drawing.Point(146, 60);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(85, 24);
            this.lblCategory.TabIndex = 13;
            this.lblCategory.Text = "Category";
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblProductName.Location = new System.Drawing.Point(146, 10);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(143, 24);
            this.lblProductName.TabIndex = 14;
            this.lblProductName.Text = "Product Name";
            // 
            // BillDetailItemUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBoxCart);
            this.Controls.Add(this.lblTotalCart);
            this.Controls.Add(this.lblPriceCart);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.lblProductName);
            this.Name = "BillDetailItemUserControl";
            this.Size = new System.Drawing.Size(768, 140);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxCart;
        private System.Windows.Forms.Label lblTotalCart;
        private System.Windows.Forms.Label lblPriceCart;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label lblProductName;
    }
}
