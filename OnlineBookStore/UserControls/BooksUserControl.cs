﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Listing books in database.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that lists the books in the database.
    /// </summary>
    public partial class BooksUserControl : UserControl
    {
        private IList<Product> Books;
        private ShoppingCart shoppingCart = Database.getInstance().getLoggedCustomer().ShoppingCart;

        /// <summary>
        /// Constructor for BooksUserControl class.
        /// </summary>
        public BooksUserControl()
        {
            InitializeComponent();
            Books = Database.getInstance().getBooks();
        }

        /// <summary>
        /// Method that fills the list with books from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BooksUserControl_Load(object sender, EventArgs e)
        {

            foreach (var book in Books)
            {
                listViewBooks.Items.Add(new ListViewItem(new string[] { ((Book)book).ISBN, book.Name, ((Book)book).Author, ((Book)book).Publisher, ((Book)book).PageNumber.ToString(), book.Price.ToString() }));
            }

        }

        /// <summary>
        /// Method that shows the picture of the selected item from the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listViewBooks_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewBooks.SelectedItems.Count > 0)
            {
                var selectedBook = Books.FirstOrDefault(b => ((Book)b).ISBN == listViewBooks.SelectedItems[0].Text);
                MemoryStream mStream = new MemoryStream();
                byte[] pData = selectedBook.Picture;
                mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                Bitmap bm = new Bitmap(mStream, false);
                mStream.Dispose();
                pictureBox2.Image = bm;

                labelBookName.Text = selectedBook.Name;
            }
        }

        /// <summary>
        /// Method that adds the book tho the shopping cart by ItemToPurchase class.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (listViewBooks.SelectedItems.Count > 0)
            {
                if (textBoxQuantity.Text != "")
                {
                    if (int.Parse(textBoxQuantity.Text) > 0)
                    {
                        var selectedBook = Books.FirstOrDefault(b => ((Book)b).ISBN == listViewBooks.SelectedItems[0].SubItems[0].Text);
                        
                        ItemToPurchase itemToPurchase = new ItemToPurchase(selectedBook, int.Parse(textBoxQuantity.Text));

                        shoppingCart.addProduct(itemToPurchase);
                        MessageBox.Show(int.Parse(textBoxQuantity.Text) + " " + selectedBook.Name + " is added successfully to the shopping cart");
                    }
                    else
                    {
                        MessageBox.Show("Quantity area can't be smaller than 1");
                    }
                }
                else
                    MessageBox.Show("Please enter a number to the quantity area");


            }
            else
                MessageBox.Show("Please select an item from the list");

        }
    }
}
