﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Add music cd to database.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that adds music cd to the database.
    /// </summary>
    public partial class AddMusicCdUserControl : UserControl
    {
        /// <summary>
        /// Constructor for AddMusicCdUserControl class.
        /// </summary>
        public AddMusicCdUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that fills the combobox with comboItems that contains MusicCdType.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddMusicCdUserControl_Load(object sender, EventArgs e)
        {
            comboMusicCdType.DataSource = new ComboItemMusicCd[] {
                new ComboItemMusicCd{ Type = MusicCdType.Country },
                new ComboItemMusicCd{ Type = MusicCdType.HardRock },
                new ComboItemMusicCd{ Type = MusicCdType.Romance }
            };
        }

        /// <summary>
        /// Method that adds a picture to the pictureBox to use it while saving to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChoosePicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.png;)|*.png;";
            if (open.ShowDialog() == DialogResult.OK)
            {
                lblFilePath.Text = open.FileName;
            }

            string image = lblFilePath.Text;
            Bitmap bmp = new Bitmap(image);
            FileStream fs = new FileStream(image, FileMode.Open, FileAccess.Read);
            byte[] bimage = new byte[fs.Length];
            fs.Read(bimage, 0, Convert.ToInt32(fs.Length));
            fs.Close();

            MemoryStream mStream = new MemoryStream();
            mStream.Write(bimage, 0, Convert.ToInt32(bimage.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            pictureBox1.Image = bm;
            pictureBox1.Visible = true;
        }

        /// <summary>
        /// Method that adds music cd to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddMusicCd_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == "" || textBoxSinger.Text == "" || textBoxPrice.Text == "" || comboMusicCdType.SelectedItem.ToString() == "" || pictureBox1.Image == null)
            {
                MessageBox.Show("Please fill all areas");
            }
            else
            {
                ComboItemMusicCd selectedType = (ComboItemMusicCd)comboMusicCdType.SelectedItem;
                MemoryStream ms = new MemoryStream();
                pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                MusicCd musicCd = (MusicCd)ProductFactory.getInstance().ProduceProduct(ProductType.MUSIC_CD);
                musicCd.Name = textBoxName.Text;
                musicCd.Singer = textBoxSinger.Text;
                musicCd.Price = float.Parse(textBoxPrice.Text);
                musicCd.Type = selectedType.Type;
                musicCd.Picture = ms.ToArray();

                Database.getInstance().addMusicCd(musicCd);
                MessageBox.Show("Music Cd successfully added to the database");
                MusicCdUserControl musicCdUserControl = new MusicCdUserControl();
                this.Controls.Clear();
                this.Controls.Add(musicCdUserControl);
            }
        }
    }
}
