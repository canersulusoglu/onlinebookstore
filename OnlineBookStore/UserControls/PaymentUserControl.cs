﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Payment methode page.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that allows to choose the payment method.
    /// </summary>
    public partial class PaymentUserControl : UserControl
    {
        private ShoppingCart shoppingCart;

        /// <summary>
        /// Constructor for PaymentUserControl class.
        /// </summary>
        public PaymentUserControl()
        {
            InitializeComponent();
            shoppingCart = Database.getInstance().getLoggedCustomer().ShoppingCart;
        }

        /// <summary>
        /// Method that changes the panel design and the checked properties of the checkboxes
        /// according to the choice of the payment method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxCash_Click(object sender, EventArgs e)
        {
            if (checkBoxCreditCard.Checked)
            {
                checkBoxCash.Checked = true;
                checkBoxCreditCard.Checked = false;
                ChangePanel();
            }
            else
            {
                checkBoxCash.Checked = true;
            }
        }

        /// <summary>
        /// Method that changes the panel design and the checked properties of the checkboxes
        /// according to the choice of the payment method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxCreditCard_Click(object sender, EventArgs e)
        {
            if (checkBoxCash.Checked)
            {
                checkBoxCreditCard.Checked = true;
                checkBoxCash.Checked = false;
                ChangePanel();
            }
            else
            {
                checkBoxCreditCard.Checked = true;
            }
        }

        /// <summary>
        /// Method that changes the panel design.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangePanel()
        {
            if (checkBoxCash.Checked)
            {
                pictureBoxPayment.Location = new Point(700, 184);
                pictureBoxPayment.Image = OnlineBookStore.Properties.Resources.Cash;
                btnFinish.Location = new Point(800, 450);
                pictureBoxTriangleCash.Visible = true;
                pictureBoxTriangleCredit.Visible = false;
                textBoxCardNumber.Visible = false;
                textBoxExpirationDate.Visible = false;
                textBoxName.Visible = false;
                textBoxSecurityCode.Visible = false;
                lblCardNumber.Visible = false;
                lblExpirationDate.Visible = false;
                lblName.Visible = false;
                lblSecurityCode.Visible = false;
            }
            else
            {
                pictureBoxPayment.Location = new Point(700, 90);
                pictureBoxPayment.Image = OnlineBookStore.Properties.Resources.CreditCard__2_;
                btnFinish.Location = new Point(880, 600);
                textBoxCardNumber.Visible = true;
                textBoxExpirationDate.Visible = true;
                textBoxName.Visible = true;
                textBoxSecurityCode.Visible = true;
                pictureBoxTriangleCash.Visible = false;
                pictureBoxTriangleCredit.Visible = true;
                lblCardNumber.Visible = true;
                lblExpirationDate.Visible = true;
                lblName.Visible = true;
                lblSecurityCode.Visible = true;
            }
        }

        /// <summary>
        /// Method to check if string contains only numeric characters.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>If string contains only digits returns true otherwise false.</returns>
        bool IsStringAllDigits(string str)
        {
            return Regex.IsMatch(str, @"^\d+$");
        }

        /// <summary>
        /// Method that lets the user make the payment.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (checkBoxCash.Checked)
            {
                shoppingCart.PaymentType = PaymentType.Cash;
                shoppingCart.placeOrder();
                MessageBox.Show("You will soon have your purchase. Thank you for shopping from us.");
                PurchasesUserControl purchasesUserControl = new PurchasesUserControl();
                this.Controls.Clear();
                this.Controls.Add(purchasesUserControl);
            }
            else
            {
                shoppingCart.PaymentType = PaymentType.CreditCard;
                string cardNumber = textBoxCardNumber.Text;
                string cardName = textBoxName.Text;
                string expirationDate = textBoxExpirationDate.Text;
                string securityCode = textBoxSecurityCode.Text;

                bool IsCardNameValid = Regex.IsMatch(cardName, @"^[a-zA-Z]+$");

                if (cardNumber.Length == 0 || cardName.Length == 0 || expirationDate.Length == 0 || securityCode.Length == 0)
                {
                    lblWarning.Text = "Please fill all the areas or confirm pay on door.";
                    return;
                }
                if (cardNumber.Length != 16 || !IsStringAllDigits(cardNumber))
                {
                    lblWarning.Text = "Please enter a valid card number";
                    return;
                }
                else if (cardName.Length < 3 || !IsCardNameValid)
                {
                    lblWarning.Text = "Please enter the name on the card.";
                    return;
                }
                else if (expirationDate.Length != 5 || !expirationDate.Contains("/"))
                {
                    lblWarning.Text = "Please enter the expiration date on the card.";
                    return;
                }
                else if (securityCode.Length != 3 || !IsStringAllDigits(securityCode))
                {
                    lblWarning.Text = "Please enter the security code on the card.";
                    return;
                }

                shoppingCart.placeOrder();

                PurchasesUserControl purchasesUserControl = new PurchasesUserControl();
                this.Controls.Clear();
                this.Controls.Add(purchasesUserControl);

            }
        }
    }
}
