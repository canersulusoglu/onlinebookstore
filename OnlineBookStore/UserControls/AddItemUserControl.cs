﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Item categories too add user control.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that allows to choose the category to add to the database.
    /// </summary>
    public partial class AddItemUserControl : UserControl
    {
        /// <summary>
        /// Constructor for AddItemUserControl class.
        /// </summary>
        public AddItemUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that opens addBookUserControl in the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelAddBook_Click(object sender, EventArgs e)
        {
            AddBookUserControl addBookUserControl = new AddBookUserControl();
            this.Controls.Clear();
            this.Controls.Add(addBookUserControl);
        }


        /// <summary>
        /// Method that opens addMagazineUserControl in the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelAddMagazine_Click(object sender, EventArgs e)
        {
            AddMagazineUserControl addMagazineUserControl = new AddMagazineUserControl();
            this.Controls.Clear();
            this.Controls.Add(addMagazineUserControl);
        }


        /// <summary>
        /// Method that opens addMusicCdUserControl in the panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelAddMusicCd_Click(object sender, EventArgs e)
        {
            AddMusicCdUserControl addMusicCdUserControl = new AddMusicCdUserControl();
            this.Controls.Clear();
            this.Controls.Add(addMusicCdUserControl);
        }
    }
}
