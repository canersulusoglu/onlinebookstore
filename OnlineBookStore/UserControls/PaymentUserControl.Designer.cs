﻿
namespace OnlineBookStore.UserControls
{
    partial class PaymentUserControl
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBoxCreditCard = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBoxCash = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFinish = new System.Windows.Forms.Button();
            this.pictureBoxPayment = new System.Windows.Forms.PictureBox();
            this.pictureBoxTriangleCash = new System.Windows.Forms.PictureBox();
            this.pictureBoxTriangleCredit = new System.Windows.Forms.PictureBox();
            this.textBoxCardNumber = new System.Windows.Forms.TextBox();
            this.lblCardNumber = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.textBoxExpirationDate = new System.Windows.Forms.TextBox();
            this.lblExpirationDate = new System.Windows.Forms.Label();
            this.textBoxSecurityCode = new System.Windows.Forms.TextBox();
            this.lblSecurityCode = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTriangleCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTriangleCredit)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(521, 660);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(68)))), ((int)(((byte)(32)))));
            this.panel3.Controls.Add(this.checkBoxCreditCard);
            this.panel3.Location = new System.Drawing.Point(0, 360);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(520, 80);
            this.panel3.TabIndex = 4;
            // 
            // checkBoxCreditCard
            // 
            this.checkBoxCreditCard.AutoSize = true;
            this.checkBoxCreditCard.Checked = true;
            this.checkBoxCreditCard.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCreditCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.checkBoxCreditCard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.checkBoxCreditCard.Location = new System.Drawing.Point(19, 22);
            this.checkBoxCreditCard.Name = "checkBoxCreditCard";
            this.checkBoxCreditCard.Size = new System.Drawing.Size(181, 36);
            this.checkBoxCreditCard.TabIndex = 1;
            this.checkBoxCreditCard.Text = "Credit Card";
            this.checkBoxCreditCard.UseVisualStyleBackColor = true;
            this.checkBoxCreditCard.Click += new System.EventHandler(this.checkBoxCreditCard_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(68)))), ((int)(((byte)(32)))));
            this.panel2.Controls.Add(this.checkBoxCash);
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.panel2.Location = new System.Drawing.Point(0, 240);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(520, 80);
            this.panel2.TabIndex = 3;
            // 
            // checkBoxCash
            // 
            this.checkBoxCash.AutoSize = true;
            this.checkBoxCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.checkBoxCash.Location = new System.Drawing.Point(20, 21);
            this.checkBoxCash.Name = "checkBoxCash";
            this.checkBoxCash.Size = new System.Drawing.Size(103, 36);
            this.checkBoxCash.TabIndex = 1;
            this.checkBoxCash.Text = "Cash";
            this.checkBoxCash.UseVisualStyleBackColor = true;
            this.checkBoxCash.Click += new System.EventHandler(this.checkBoxCash_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(13, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(477, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "How Would You Like To Pay?";
            // 
            // btnFinish
            // 
            this.btnFinish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.btnFinish.FlatAppearance.BorderSize = 0;
            this.btnFinish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinish.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnFinish.Location = new System.Drawing.Point(774, 498);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(182, 47);
            this.btnFinish.TabIndex = 3;
            this.btnFinish.Text = "Finish Payment";
            this.btnFinish.UseVisualStyleBackColor = false;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // pictureBoxPayment
            // 
            this.pictureBoxPayment.Image = global::OnlineBookStore.Properties.Resources.CreditCard__2_;
            this.pictureBoxPayment.Location = new System.Drawing.Point(641, 76);
            this.pictureBoxPayment.Name = "pictureBoxPayment";
            this.pictureBoxPayment.Size = new System.Drawing.Size(338, 188);
            this.pictureBoxPayment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPayment.TabIndex = 2;
            this.pictureBoxPayment.TabStop = false;
            // 
            // pictureBoxTriangleCash
            // 
            this.pictureBoxTriangleCash.Image = global::OnlineBookStore.Properties.Resources.Triangle;
            this.pictureBoxTriangleCash.Location = new System.Drawing.Point(520, 254);
            this.pictureBoxTriangleCash.Name = "pictureBoxTriangleCash";
            this.pictureBoxTriangleCash.Size = new System.Drawing.Size(44, 50);
            this.pictureBoxTriangleCash.TabIndex = 1;
            this.pictureBoxTriangleCash.TabStop = false;
            this.pictureBoxTriangleCash.Visible = false;
            // 
            // pictureBoxTriangleCredit
            // 
            this.pictureBoxTriangleCredit.Image = global::OnlineBookStore.Properties.Resources.Triangle;
            this.pictureBoxTriangleCredit.Location = new System.Drawing.Point(519, 372);
            this.pictureBoxTriangleCredit.Name = "pictureBoxTriangleCredit";
            this.pictureBoxTriangleCredit.Size = new System.Drawing.Size(44, 50);
            this.pictureBoxTriangleCredit.TabIndex = 1;
            this.pictureBoxTriangleCredit.TabStop = false;
            // 
            // textBoxCardNumber
            // 
            this.textBoxCardNumber.Location = new System.Drawing.Point(774, 308);
            this.textBoxCardNumber.Name = "textBoxCardNumber";
            this.textBoxCardNumber.Size = new System.Drawing.Size(182, 22);
            this.textBoxCardNumber.TabIndex = 4;
            // 
            // lblCardNumber
            // 
            this.lblCardNumber.AutoSize = true;
            this.lblCardNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.lblCardNumber.Location = new System.Drawing.Point(664, 313);
            this.lblCardNumber.Name = "lblCardNumber";
            this.lblCardNumber.Size = new System.Drawing.Size(92, 17);
            this.lblCardNumber.TabIndex = 5;
            this.lblCardNumber.Text = "Card Number";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(774, 350);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(182, 22);
            this.textBoxName.TabIndex = 4;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.lblName.Location = new System.Drawing.Point(664, 355);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name";
            // 
            // textBoxExpirationDate
            // 
            this.textBoxExpirationDate.Location = new System.Drawing.Point(774, 390);
            this.textBoxExpirationDate.Name = "textBoxExpirationDate";
            this.textBoxExpirationDate.Size = new System.Drawing.Size(182, 22);
            this.textBoxExpirationDate.TabIndex = 4;
            // 
            // lblExpirationDate
            // 
            this.lblExpirationDate.AutoSize = true;
            this.lblExpirationDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.lblExpirationDate.Location = new System.Drawing.Point(664, 395);
            this.lblExpirationDate.Name = "lblExpirationDate";
            this.lblExpirationDate.Size = new System.Drawing.Size(104, 17);
            this.lblExpirationDate.TabIndex = 5;
            this.lblExpirationDate.Text = "Expiration Date";
            // 
            // textBoxSecurityCode
            // 
            this.textBoxSecurityCode.Location = new System.Drawing.Point(774, 437);
            this.textBoxSecurityCode.Name = "textBoxSecurityCode";
            this.textBoxSecurityCode.Size = new System.Drawing.Size(182, 22);
            this.textBoxSecurityCode.TabIndex = 4;
            // 
            // lblSecurityCode
            // 
            this.lblSecurityCode.AutoSize = true;
            this.lblSecurityCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.lblSecurityCode.Location = new System.Drawing.Point(664, 440);
            this.lblSecurityCode.Name = "lblSecurityCode";
            this.lblSecurityCode.Size = new System.Drawing.Size(96, 17);
            this.lblSecurityCode.TabIndex = 5;
            this.lblSecurityCode.Text = "Security Code";
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
            this.lblWarning.Location = new System.Drawing.Point(803, 592);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(46, 17);
            this.lblWarning.TabIndex = 6;
            this.lblWarning.Text = "label2";
            // 
            // PaymentUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(68)))), ((int)(((byte)(32)))));
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.lblSecurityCode);
            this.Controls.Add(this.lblExpirationDate);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblCardNumber);
            this.Controls.Add(this.textBoxSecurityCode);
            this.Controls.Add(this.textBoxExpirationDate);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.textBoxCardNumber);
            this.Controls.Add(this.btnFinish);
            this.Controls.Add(this.pictureBoxPayment);
            this.Controls.Add(this.pictureBoxTriangleCash);
            this.Controls.Add(this.pictureBoxTriangleCredit);
            this.Controls.Add(this.panel1);
            this.Name = "PaymentUserControl";
            this.Size = new System.Drawing.Size(1060, 660);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTriangleCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTriangleCredit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBoxTriangleCredit;
        private System.Windows.Forms.PictureBox pictureBoxTriangleCash;
        private System.Windows.Forms.PictureBox pictureBoxPayment;
        private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.TextBox textBoxCardNumber;
        private System.Windows.Forms.Label lblCardNumber;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox textBoxExpirationDate;
        private System.Windows.Forms.Label lblExpirationDate;
        private System.Windows.Forms.TextBox textBoxSecurityCode;
        private System.Windows.Forms.Label lblSecurityCode;
        private System.Windows.Forms.CheckBox checkBoxCreditCard;
        private System.Windows.Forms.CheckBox checkBoxCash;
        private System.Windows.Forms.Label lblWarning;
    }
}
