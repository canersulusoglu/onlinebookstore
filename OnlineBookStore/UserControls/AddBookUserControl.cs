﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Add book to database.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// Class that adds book to the database.
    /// </summary>
    public partial class AddBookUserControl : UserControl
    {
        /// <summary>
        /// Constructor for AddBookUserControl class.
        /// </summary>
        public AddBookUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that adds book to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddBook_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == null || textBoxPrice.Text == null || textBoxISBN.Text == null || textBoxAuthor.Text == null || textBoxPublisher.Text == null || textBoxPageNumber.Text == null)
            {
                MessageBox.Show("Please fill all areas");
            }
            else
            {
                MemoryStream ms = new MemoryStream();
                pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                Book book = (Book)ProductFactory.getInstance().ProduceProduct(ProductType.BOOK);
                book.ISBN = textBoxISBN.Text;
                book.Author = textBoxAuthor.Text;
                book.Name = textBoxName.Text;
                book.PageNumber = int.Parse(textBoxPageNumber.Text);
                book.Price = float.Parse(textBoxPrice.Text);
                book.Publisher = textBoxPublisher.Text;
                book.Picture = ms.ToArray();

                Database.getInstance().addBook(book);
                MessageBox.Show("Book successfully added to the database");
                BooksUserControl booksUserControl = new BooksUserControl();
                this.Controls.Clear();
                this.Controls.Add(booksUserControl);
            }

        }

        /// <summary>
        /// Method that adds a picture to the pictureBox to use it while saving to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChoosePicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.png)|*.png";
            if (open.ShowDialog() == DialogResult.OK)
            {
                lblFilePath.Text = open.FileName;
            }

            string image = lblFilePath.Text;
            Bitmap bmp = new Bitmap(image);
            FileStream fs = new FileStream(image, FileMode.Open, FileAccess.Read);
            byte[] bimage = new byte[fs.Length];
            fs.Read(bimage, 0, Convert.ToInt32(fs.Length));
            fs.Close();

            MemoryStream mStream = new MemoryStream();
            mStream.Write(bimage, 0, Convert.ToInt32(bimage.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            pictureBox1.Image = bm;
            pictureBox1.Visible = true;
        }
    }
}
