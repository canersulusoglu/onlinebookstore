﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 02.06.2021
*  @brief   : Items for shopping cart
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    /// <summary>
    /// An item for listing the products in the shopping cart.
    /// </summary>
    public partial class ShoppingCartItemUserControl : UserControl
    {
        private ShoppingCart shoppingCart;
        private ItemToPurchase itemToPurchase;

        private void ShoppingCartItemUserControl_Load(object sender, EventArgs e)
        {
            textBoxQuantityCart.Text = Quantity.ToString();
            shoppingCart = Database.getInstance().getLoggedCustomer().ShoppingCart;
            itemToPurchase = shoppingCart.ItemToPurchases.Find(x => x.Product.ID == Id);
        }

        /// <summary>
        /// Constructor for ShoppingCartItemUserControl class.
        /// </summary>
        public ShoppingCartItemUserControl()
        {
            InitializeComponent();
        }

        #region Properties

        private string _id;
        private int _quantity;
        private string _name;
        private Image _picture;
        private string _price;
        private string _category;
        private string _total;


        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                lblProductName.Text = value;
            }
        }

        public Image Picture
        {
            get { return _picture; }
            set
            {
                _picture = value;
                pictureBoxCart.Image = value;
            }
        }


        public string Price
        {
            get { return _price; }
            set
            {
                _price = value;
                lblPriceCart.Text = value;
            }
        }

        public string Total
        {
            get { return _total; }
            set
            {
                _total = value;
                lblTotalCart.Text = value;
            }
        }

        public string Category
        {
            get { return _category; }
            set
            {
                _category = value;
                lblCategory.Text = value;
            }
        }

        #endregion


        /// <summary>
        /// Method that increase the quantity of the product in the shoping cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnIncrease_Click(object sender, EventArgs e)
        {
            Quantity += 1;
            textBoxQuantityCart.Text = Quantity.ToString();
            itemToPurchase.Quantity += 1;
            lblTotalCart.Text = (Quantity * float.Parse(Price)).ToString();
        }

        /// <summary>
        /// Method that decrease the quantity of the product in the shoping cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDecrease_Click(object sender, EventArgs e)
        {
            if (Quantity != 1)
            {
                Quantity -= 1;
                textBoxQuantityCart.Text = Quantity.ToString();
                itemToPurchase.Quantity -= 1;
                lblTotalCart.Text = (Quantity * float.Parse(Price)).ToString();
            }
            else
            {
                DialogResult res = MessageBox.Show("Are you sure you want to remove the product?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (res == DialogResult.OK)
                {
                    shoppingCart.removeProduct(itemToPurchase);
                    this.Visible = false;
                    MessageBox.Show("Successfully removed from the shopping cart");
                }

            }

        }

        /// <summary>
        /// Method that removes the product from the shopping cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabelRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult res = MessageBox.Show("Are you sure you want to remove the product?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res == DialogResult.OK)
            {
                shoppingCart.removeProduct(itemToPurchase);
                this.Visible = false;
                MessageBox.Show("Successfully removed from the shopping cart");
            }
        }
    }
}
