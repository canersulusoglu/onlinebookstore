﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore.UserControls
{
    public partial class BillDetailItemUserControl : UserControl
    {
        public BillDetailItemUserControl()
        {
            InitializeComponent();
        }

        #region Properties

        private string _id;
        private int _quantity;
        private string _name;
        private Image _picture;
        private string _price;
        private string _category;
        private string _total;


        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                lblProductName.Text = value;
            }
        }

        public Image Picture
        {
            get { return _picture; }
            set
            {
                _picture = value;
                pictureBoxCart.Image = value;
            }
        }


        public string Price
        {
            get { return _price; }
            set
            {
                _price = value;
                lblPriceCart.Text = value;
            }
        }

        public string Total
        {
            get { return _total; }
            set
            {
                _total = value;
                lblTotalCart.Text = value;
            }
        }

        public string Category
        {
            get { return _category; }
            set
            {
                _category = value;
                lblCategory.Text = value;
            }
        }

        #endregion
    }
}
