﻿/**
*  @author  : Süleyman Kanal
*  @number  : 152120181007
*  @mail    : suleyman-kanal@hotmail.com
*  @date    : 01.06.2020
*  @brief   : Main window of the application
*/

using OnlineBookStore.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore
{
    public partial class windowHome : Form
    {
        int navWidth;
        bool isCollapsed;
        private Customer loggedCustomer;

        /// <summary>
        /// Constructor for windowHome class.
        /// </summary>
        public windowHome()
        {
            InitializeComponent();
            pictureBox5.Visible = false;
            btnAddItem.Visible = false;
            HomeUserControl homeUserControl = new HomeUserControl();
            addUserControlToPanel(homeUserControl);
            navWidth = panelSideNav.Width;
            isCollapsed = false;
            loggedCustomer = Database.getInstance().getLoggedCustomer();
            if (loggedCustomer.Rank == CustomerRank.Admin)
            {
                pictureBox5.Visible = true;
                btnAddItem.Visible = true;
            }
        }

        /// <summary>
        /// Method that stops the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
            Application.Exit();
        }

        /// <summary>
        /// Method that helps to animate the sideNav panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                panelSideNav.Width += 20;
                if (panelSideNav.Width >= navWidth)
                {
                    logo.Width = 120;
                    logo.Height = 90;
                    logo.Left = 52;
                    logo.Top = 12;
                    brand.Visible = true;
                    timer1.Stop();
                    isCollapsed = false;
                    this.Refresh();
                }
            }
            else
            {
                panelSideNav.Width -= 20;
                if (panelSideNav.Width <= 80)
                {
                    logo.Width = 60;
                    logo.Height = 45;
                    logo.Left = 10;
                    logo.Top = 50;
                    brand.Visible = false;
                    timer1.Stop();
                    isCollapsed = true;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Method that opens or closes the side nav panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenu_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        /// <summary>
        /// Method that moves the decoration of the buttons.
        /// </summary>
        /// <param name="button"></param>
        private void movePanelBtnSide(Control button)
        {
            panelBtnSide.Top = button.Top;
            panelBtnSide.Height = button.Height;

        }

        /// <summary>
        /// Method that fills the panel with user control.
        /// </summary>
        /// <param name="c"></param>
        private void addUserControlToPanel(Control c)
        {
            c.Dock = DockStyle.Fill;
            panelUserControls.Controls.Clear();
            panelUserControls.Controls.Add(c);
        }

        /// <summary>
        /// Method that fills the panel with HomeUserControl.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHome_Click(object sender, EventArgs e)
        {
            movePanelBtnSide(btnHome);
            HomeUserControl homeUserControl = new HomeUserControl();
            addUserControlToPanel(homeUserControl);
        }

        /// <summary>
        /// Method that fills the panel with CategoriesUserControl.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCategories_Click(object sender, EventArgs e)
        {
            movePanelBtnSide(btnCategories);
            CategoriesUserControl categoriesUserControl = new CategoriesUserControl();
            addUserControlToPanel(categoriesUserControl);
        }

        /// <summary>
        /// Method that fills the panel with PurchasesUserControl.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPurchases_Click(object sender, EventArgs e)
        {
            movePanelBtnSide(btnPurchases);
            PurchasesUserControl purchasesUserControl = new PurchasesUserControl();
            addUserControlToPanel(purchasesUserControl);
        }

        /// <summary>
        /// Method that fills the panel with ShoppingCartUserControl.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCart_Click(object sender, EventArgs e)
        {
            movePanelBtnSide(btnCart);
            ShoppingCartUserControl shoppingCartUserControl = new ShoppingCartUserControl();
            addUserControlToPanel(shoppingCartUserControl);
        }

        /// <summary>
        /// Method that fills the panel with AddItemUserControl.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddItem_Click(object sender, EventArgs e)
        {
            movePanelBtnSide(btnAddItem);
            AddItemUserControl addItemUserControl = new AddItemUserControl();
            addUserControlToPanel(addItemUserControl);
        }
    }
}
