﻿/**
*  @author  : Caner Sülüşoğlu
*  @number  : 152120181022
*  @mail    : cscaner26@gmail.com
*  @date    : 30.05.2021
*  @brief   : Bill data class.
*/
using System;

namespace OnlineBookStore
{
    /// <summary>
    /// Class that store bill data.
    /// </summary>
    class Bill
    {
        protected string id;
        protected DateTime date;
        protected float totalPrice;

        /// <summary>
        /// Bill id.
        /// </summary>
        public string Id { get { return this.id; } }

        /// <summary>
        /// Bill date.
        /// </summary>
        public DateTime Date { get { return this.date; } }

        /// <summary>
        /// Bill total price.
        /// </summary>
        public float TotalPrice { get { return this.totalPrice; } }

        /// <summary>
        /// Bill class constructor.
        /// </summary>
        /// <param name="_id">Bill id.</param>
        /// <param name="_date">Bill date.</param>
        /// <param name="_totalPrice">Bill total price.</param>
        public Bill(string _id, DateTime _date, float _totalPrice)
        {
            this.id = _id;
            this.date = _date;
            this.totalPrice = _totalPrice;
        }
    }
}
