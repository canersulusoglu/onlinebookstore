﻿/**
 * @author  : Metin Cem Demirdaş
 * @number  : 152120181015
 * @mail    : cem.dmrds@hotmail.com
 * @date    : 04/06/2021
 * @brief   : class for the inherited product "Magazine".
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{
    enum MagazineType
    {
        Actual,
        News,
        Sport,
        Computer,
        Technology
    }
    /// <summary>
    /// this class holds Magazine and its specific attributes. 
    /// </summary>
    class Magazine : Product
    {
        private string issue;
        private MagazineType type;

        public string Issue { get { return this.issue; } set { this.issue = value; } }
        public MagazineType Type { get { return this.type; } set { this.type = value; } }
        /// <summary>
        /// Constructor of the Magazine class
        /// </summary>
        public Magazine()
        {
            this.productType = ProductType.MAGAZINE;
        }
        /// <summary>
        /// This determines the type of the Magazine
        /// </summary>
        /// <returns> the magazine type(string) based on Enum(MagazineType)</returns>
        public string getTypeString()
        {
            switch (this.type)
            {
                case MagazineType.Actual:
                    return "Actual";
                case MagazineType.News:
                    return "News";
                case MagazineType.Sport:
                    return "Sport";
                case MagazineType.Computer:
                    return "Computer";
                case MagazineType.Technology:
                    return "Technology";
                default:
                    return "";
            }
        }
        /// <summary>
        /// Prints all of the Magazine's information
        /// </summary>
        public override void printProperties()
        {
            Console.Write("Id: " + this.id + " \n" + "Name: " + this.name + " \n" + "Price: " + this.price.ToString() + " \n" + "Issue: " + this.issue + " \n" + "Type: " + this.getTypeString() + " \n");
        }
    }
}
