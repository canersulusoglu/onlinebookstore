﻿/**
 * @author  : Metin Cem Demirdaş
 * @number  : 152120181015
 * @mail    : cem.dmrds@hotmail.com
 * @date    : 04/06/2021
 * @brief   : this class helps us to apply factory and singleton patterns 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{
    enum ProductType
    {
        BOOK,
        MAGAZINE,
        MUSIC_CD
    }

    /// <summary>
    /// this class has both factory(for the products) and singleton patterns. 
    /// </summary>
    class ProductFactory
    {
        private static ProductFactory instance;
        private static object lockObject = new Object();
        /// <summary>
        /// default constructor. 
        /// </summary>
        private ProductFactory() { }
        /// <summary>
        /// the method for the singleton pattern.
        /// </summary>
        /// <returns>instance</returns>
        public static ProductFactory getInstance()
        {
            lock (lockObject)
            {
                if (instance == null)
                {
                    instance = new ProductFactory();
                }
                return instance;
            }
        }
        /// <summary>
        /// the method for the factory pattern.
        /// </summary>
        /// <param name="productType">used to determine which product to be created based on the Enum(ProductType)</param>
        /// <returns>the product based on Enum</returns>
        public Product ProduceProduct(ProductType productType)
        {
            Product newProduct = null;
            switch (productType)
            {
                case ProductType.BOOK:
                    newProduct = new Book();
                    break;
                case ProductType.MAGAZINE:
                    newProduct = new Magazine();
                    break;
                case ProductType.MUSIC_CD:
                    newProduct = new MusicCd();
                    break;
            }
            return newProduct;
        }
    }
}
