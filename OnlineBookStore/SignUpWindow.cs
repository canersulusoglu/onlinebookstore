﻿/**
*  @author  : Tarık Kemal Gündoğdu
*  @number  : 152120191054
*  @mail    : tarikkemal314@gmail.com
*  @date    : 3.06.2021
*  @brief   : Sign Up Window.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore
{
    /// <summary>
    /// Sign Up Window form page.
    /// </summary>
    public partial class SignUpWindow : Form
    {
        Database db = Database.getInstance();
        Timer timer = new Timer();
        int timeLeft = 1;

        Customer customer;

        /// <summary>
        /// Sign Up Window constructor.
        /// </summary>
        public SignUpWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that checks if string is in email format.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>If string is in email format return true otherwise false.</returns>
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Method that registers a new user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signUpBtn_Click(object sender, EventArgs e)
        {
            string name = nameText.Text;
            string address = addressText.Text;
            string userName = userNameText.Text;
            string email = emailText.Text;
            string password = passwordText.Text;

            customer = new Customer(name, address, email, userName, password, 0);

            if (nameText.TextLength < 1 || addressText.TextLength < 1 || userNameText.TextLength < 1|| emailText.TextLength < 1|| passwordText.TextLength < 1)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Please fill all the areas.";
                return;
            }

            if (userNameText.TextLength < 6)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Username must be more than 6 characters.";
                return;
            }
            else if (!IsValidEmail(email) || emailText.TextLength < 1)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "You must enter a valid email address.";
                return;
            }
            else if (passwordText.TextLength < 6)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Password must be more than 6 characters.";
                return;
            }

            successLabel.Text = "Wait please...";
            successLabel.ForeColor = Color.Green;
           
            messageLabel.Visible = false;

            timer.Interval = 1000;
            timer.Enabled = true;

            timer.Tick += timer_Tick;
        }

        /// <summary>
        /// Method that counts down the timer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            timeLeft -= 1;

            if (timeLeft == 0)
            {
                successLabel.Visible = false;
                db.Register(customer);
                timer.Stop();
            }
        }

        /// <summary>
        /// Method that returns the user back to the LogIn window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backToLogInBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.FormClosed += (s, args) =>
            {
                this.Close();
            };
            loginWindow.Show();
        }
    }
}
