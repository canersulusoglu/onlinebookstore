﻿/**
 * @author  : Metin Cem Demirdaş
 * @number  : 152120181015
 * @mail    : cem.dmrds@hotmail.com
 * @date    : 04/06/2021
 * @brief   : class for the inherited product "Book".
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{
    /// <summary>
    /// this class holds Book and its specific attributes. 
    /// </summary>
    class Book : Product
    {
        private string isbn;
        private string author;
        private string publisher;
        private int pageNumber;

        public string ISBN { get { return this.isbn; } set { this.isbn = value; } }
        public string Author { get { return this.author; } set { this.author = value; } }
        public string Publisher { get { return this.publisher; } set { this.publisher = value; } }
        public int PageNumber { get { return this.pageNumber; } set { this.pageNumber = value; } }
        /// <summary>
        /// Constructor of the Book class
        /// </summary>
        public Book()
        {
            this.productType = ProductType.BOOK;
        }
        /// <summary>
        /// Prints all of the Book's information
        /// </summary>
        public override void printProperties()
        {
            Console.Write("Id: " + this.id + " \n" + "Name: " + this.name + " \n" + "Price: " + this.price.ToString() + " \n" + "ISBN: " + this.isbn + " \n" + "Author: " + this.author + " \n" + "Publisher: " + this.publisher + " \n" + "Page number: " + this.pageNumber + " \n");
        }
    }
}
