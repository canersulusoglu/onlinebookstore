﻿/**
*  @author  : Caner Sülüşoğlu
*  @number  : 152120181022
*  @mail    : cscaner26@gmail.com
*  @date    : 30.05.2021
*  @brief   : Database operations.
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace OnlineBookStore
{
    /// <summary>
    /// Class that connects database to use database more efficient.
    /// </summary>
    class Database
    {
        private SqlConnection SqlConnection;
        private DataTable dataTable, dataTable2;

        private List<Customer> Customers;
        private List<Product> Products;
        private List<Bill> Bills;

        private Customer loggedCustomer;

        private static Database instance;
        private static object lockObject = new Object();

        /// <summary>
        /// Get Database instance.
        /// </summary>
        /// <returns>Database</returns>
        public static Database getInstance()
        {
            lock (lockObject)
            {
                if (instance == null)
                {
                    instance = new Database();
                }
                return instance;
            }
        }

        /// <summary>
        /// Constructor for Database class.
        /// </summary>
        private Database() {
            dataTable = new DataTable();
            dataTable2 = new DataTable();
            Customers = new List<Customer>();
            Products = new List<Product>();
            Bills = new List<Bill>();
            DatabaseConnection();
        }

        /// <summary>
        /// Database connection configuration.
        /// </summary>
        private void DatabaseConnection()
        {
            var connectionString = @"Data Source=SQL5097.site4now.net;Initial Catalog=db_a75045_onlinebookstore;User Id=db_a75045_onlinebookstore_admin;Password=Database4769";
            SqlConnection = new SqlConnection(connectionString);
        }

        #region About Customer

        /// <summary>
        /// Login method for customer to login to application.
        /// </summary>
        /// <param name="username">Customer username.</param>
        /// <param name="password">Customer password</param>
        /// <returns>If user succesfully login true, otherwise false.</returns>
        public bool Login(string username, string password)
        {
            try
            {
                dataTable.Clear();
                SqlConnection.Open();
                string query = @"Select * from Customers where username='" + username + "' and password='" + password + "'";
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                command.Fill(dataTable);
                SqlConnection.Close();

                if (dataTable.Rows.Count == 1)
                {
                    DataRow customerData = dataTable.Rows[0];
                    loggedCustomer = new Customer
                    (
                        customerData["id"].ToString(),
                        customerData["name"].ToString(),
                        customerData["adress"].ToString(),
                        customerData["email"].ToString(),
                        customerData["username"].ToString(),
                        customerData["password"].ToString(),
                        (CustomerRank)customerData["rank"]
                    );
					return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Logout method for customer to logout from application.
        /// </summary>
        public void LogOut()
        {
            loggedCustomer = null;
            Customers.Clear();
            Products.Clear();
            Bills.Clear();
        }

        /// <summary>
        /// Register method that adds customer data to database.
        /// </summary>
        /// <param name="customer">Parameter that stores information about customer that will register.</param>
        public void Register(Customer customer)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Insert into Customers(name,adress,email,username,password) values(@name,@adress,@email,@username,@password)";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", customer.Name);
                command.Parameters.AddWithValue("@adress", customer.Address);
                command.Parameters.AddWithValue("@email", customer.Email);
                command.Parameters.AddWithValue("@username", customer.Username);
                command.Parameters.AddWithValue("@password", customer.Password);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Customer has registered successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SqlException error)
            {
                string errorMessage;
                switch (error.Number)
                {
                    case 2601:
                        errorMessage = "This username or email is already exists!";
                        break;
                    default:
                        errorMessage = error.Message;
                        break;
                }
                MessageBox.Show(errorMessage, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Gets informations about logged customer if customer has been logged.
        /// </summary>
        /// <returns>If customer has been logged returns customer datas, otherwise null.</returns>
        public Customer getLoggedCustomer()
        {
            if (loggedCustomer != null)
            {
                return loggedCustomer;
            }
            return null;
        }

        #endregion

        #region Get Data From Database

        /// <summary>
        /// Method that pulls all customer data from database.
        /// </summary>
        /// <returns>List of customers data.</returns>
        public List<Customer> getCustomers()
        {
            try
            {
                Customers.Clear();
                dataTable.Clear();
                SqlConnection.Open();
                string query = @"Select * From Customers";
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                command.Fill(dataTable);
                SqlConnection.Close();
                foreach (DataRow customer in dataTable.Rows)
                {
                    Customer newCustomer = new Customer
                    (
                        customer["id"].ToString(),
                        customer["name"].ToString(),
                        customer["adress"].ToString(),
                        customer["email"].ToString(),
                        customer["username"].ToString(),
                        customer["password"].ToString(),
                        (CustomerRank)customer["rank"]
                    );
                    Customers.Add(newCustomer);
                }
                return Customers;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        /// <summary>
        /// Method that pulls all books data from database.
        /// </summary>
        /// <returns>List of books data.</returns>
        public List<Product> getBooks()
        {
            try
            {
                Products.Clear();
                dataTable.Clear();
                SqlConnection.Open();
                string query = @"Select * From Books";
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                command.Fill(dataTable);
                SqlConnection.Close();

                foreach (DataRow product in dataTable.Rows)
                {
                    Book newBook = (Book)ProductFactory.getInstance().ProduceProduct(ProductType.BOOK);
                    newBook.ID = product["id"].ToString();
                    newBook.Name = product["name"].ToString();
                    newBook.Price = float.Parse(product["price"].ToString());
                    newBook.Picture = (byte[])product["picture"];
                    newBook.ISBN = product["ISBN"].ToString();
                    newBook.Author = product["author"].ToString();
                    newBook.Publisher = product["publisher"].ToString();
                    newBook.PageNumber = (int)product["pageNumber"];

                    Products.Add(newBook);
                }
                return Products;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        /// <summary>
        /// Method that pulls all magazines data from database.
        /// </summary>
        /// <returns>List of magazines data</returns>
        public List<Product> getMagazines()
        {
            try
            {
                Products.Clear();
                dataTable.Clear();
                SqlConnection.Open();
                string query = @"Select * From Magazines";
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                command.Fill(dataTable);
                SqlConnection.Close();
                foreach (DataRow product in dataTable.Rows)
                {
                    Magazine newMagazine = (Magazine)ProductFactory.getInstance().ProduceProduct(ProductType.MAGAZINE);
                    newMagazine.ID = product["id"].ToString();
                    newMagazine.Name = product["name"].ToString();
                    newMagazine.Price = float.Parse(product["price"].ToString());
                    newMagazine.Picture = (byte[])product["picture"];
                    newMagazine.Issue = product["issue"].ToString();
                    newMagazine.Type = (MagazineType)product["type"];

                    Products.Add(newMagazine);
                }
                return Products;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        /// <summary>
        /// Method that pulls all music cds data from database.
        /// </summary>
        /// <returns>List of music cds data</returns>
        public List<Product> getMusicCds()
        {
            try
            {
                Products.Clear();
                dataTable.Clear();
                SqlConnection.Open();
                string query = @"Select * From MusicCds";
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                command.Fill(dataTable);
                SqlConnection.Close();
                foreach (DataRow product in dataTable.Rows)
                {
                    MusicCd newMusicCd = (MusicCd)ProductFactory.getInstance().ProduceProduct(ProductType.MUSIC_CD);
                    newMusicCd.ID = product["id"].ToString();
                    newMusicCd.Name = product["name"].ToString();
                    newMusicCd.Price = float.Parse(product["price"].ToString());
                    newMusicCd.Picture = (byte[])product["picture"];
                    newMusicCd.Singer = product["singer"].ToString();
                    newMusicCd.Type = (MusicCdType)product["type"];

                    Products.Add(newMusicCd);
                }
                return Products;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        #endregion

        #region Add Data in Database

        /// <summary>
        /// Method that adds new book data to database.
        /// </summary>
        /// <param name="book">Book that will add to database.</param>
        public void addBook(Book book)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Insert into Books(name,price,picture,ISBN,author,publisher,pageNumber) values(@name,@price,@picture,@ISBN,@author,@publisher,@pageNumber)";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", book.Name);
                command.Parameters.AddWithValue("@price", book.Price);
                command.Parameters.AddWithValue("@picture", book.Picture);
                command.Parameters.AddWithValue("@ISBN", book.ISBN);
                command.Parameters.AddWithValue("@author", book.Author);
                command.Parameters.AddWithValue("@publisher", book.Publisher);
                command.Parameters.AddWithValue("@pageNumber", book.PageNumber);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Book added.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Method that adds new magazine data to database.
        /// </summary>
        /// <param name="book">Magazine that will add to database.</param>
        public void addMagazine(Magazine magazine)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Insert into Magazines(name,price,picture,issue,type) values(@name,@price,@picture,@issue,@type)";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", magazine.Name);
                command.Parameters.AddWithValue("@price", magazine.Price);
                command.Parameters.AddWithValue("@picture", magazine.Picture);
                command.Parameters.AddWithValue("@issue", magazine.Issue);
                command.Parameters.AddWithValue("@type", (int)magazine.Type);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Magazine added.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Method that adds new music cd data to database.
        /// </summary>
        /// <param name="book">Music Cd that will add to database.</param>
        public void addMusicCd(MusicCd musicCd)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Insert into MusicCds(name,price,picture,singer,type) values(@name,@price,@picture,@singer,@type)";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", musicCd.Name);
                command.Parameters.AddWithValue("@price", musicCd.Price);
                command.Parameters.AddWithValue("@picture", musicCd.Picture);
                command.Parameters.AddWithValue("@singer", musicCd.Singer);
                command.Parameters.AddWithValue("@type", (int)musicCd.Type);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Music Cd added.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Update Data in Database

        /// <summary>
        /// Method that updates customer data at database.
        /// </summary>
        /// <param name="book">Customer that will update.</param>
        public void updateCustomer(Customer customer)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Update Customers set name=@name,adress=@adress,email=@email,username=@username,password=@password where id=@customerId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", customer.Name);
                command.Parameters.AddWithValue("@adress", customer.Address);
                command.Parameters.AddWithValue("@email", customer.Email);
                command.Parameters.AddWithValue("@username", customer.Username);
                command.Parameters.AddWithValue("@password", customer.Password);
                command.Parameters.AddWithValue("@customerId", customer.ID);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Customer has been updated.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Method that updates book data at database.
        /// </summary>
        /// <param name="book">Book that will update.</param>
        public void updateBook(Book book)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Update Books set name=@name,price=@price,picture=@picture,ISBN=@ISBN,author=@author,publisher=@publisher,pageNumber=@pageNumber where id=@bookId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", book.Name);
                command.Parameters.AddWithValue("@price", book.Price);
                command.Parameters.AddWithValue("@picture", book.Picture);
                command.Parameters.AddWithValue("@ISBN", book.ISBN);
                command.Parameters.AddWithValue("@author", book.Author);
                command.Parameters.AddWithValue("@publisher", book.Publisher);
                command.Parameters.AddWithValue("@pageNumber", book.PageNumber);
                command.Parameters.AddWithValue("@bookId", book.ID);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Book has been updated.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Method that updates magazine data at database.
        /// </summary>
        /// <param name="book">Magazine that will update.</param>
        public void updateMagazine(Magazine magazine)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Update Magazines set name=@name,price=@price,picture=@picture,issue=@issue,type=@type where id=@magazineId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", magazine.Name);
                command.Parameters.AddWithValue("@price", magazine.Price);
                command.Parameters.AddWithValue("@picture", magazine.Picture);
                command.Parameters.AddWithValue("@issue", magazine.Issue);
                command.Parameters.AddWithValue("@type", magazine.Type);
                command.Parameters.AddWithValue("@magazineId", magazine.ID);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Magazine has been updated.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Method that updates music cd data at database.
        /// </summary>
        /// <param name="book">Music Cd that will update.</param>
        public void updateMusicCd(MusicCd musicCd)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Update MusicCds set name=@name,price=@price,picture=@picture,singer=@singer,type=@type where id=@musicCdId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", musicCd.Name);
                command.Parameters.AddWithValue("@price", musicCd.Price);
                command.Parameters.AddWithValue("@picture", musicCd.Picture);
                command.Parameters.AddWithValue("@singer", musicCd.Singer);
                command.Parameters.AddWithValue("@type", musicCd.Type);
                command.Parameters.AddWithValue("@musicCdId", musicCd.ID);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Music Cd has been updated.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Delete Data in Database

        /// <summary>
        /// Method that deletes book from database.
        /// </summary>
        /// <param name="bookId">Id that is book will be deleted.</param>
        public void deleteBook(int bookId)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Delete from Books where id=@bookId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@bookId", bookId);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Book has been deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Method that deletes magazine from database.
        /// </summary>
        /// <param name="magazineId">Id that is magazine will be deleted.</param>
        public void deleteMagazine(int magazineId)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Delete from Magazines where id=@magazineId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@magazineId", magazineId);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Magazine has been deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Method that deletes music cd from database.
        /// </summary>
        /// <param name="musicCdId">Id that is music cd will be deleted.</param>
        public void deleteMusicCd(int musicCdId)
        {
            try
            {
                SqlConnection.Open();
                string query = @"Delete from MusicCds where id=@musicCdId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@musicCdId", musicCdId);
                command.ExecuteNonQuery();
                SqlConnection.Close();
                MessageBox.Show("Music Cd has been deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Bills 

        /// <summary>
        /// Method that gets all bills that belongs to customer.
        /// </summary>
        /// <param name="customerId">Customer id.</param>
        /// <returns>List of bills data.</returns>
        public List<Bill> getBills(int customerId)
        {
            try
            {
                Bills.Clear();
                dataTable.Clear();
                SqlConnection.Open();
                string query = @"Select * from Bills where customerId=" + customerId.ToString();
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                command.Fill(dataTable);
                SqlConnection.Close();

                foreach (DataRow bill in dataTable.Rows)
                {
                    Bill newBill = new Bill
                        (
                            bill["id"].ToString(),
                            (DateTime)bill["date"],
                            float.Parse(bill["totalPrice"].ToString())
                        );
                    Bills.Add(newBill);
                }
                return Bills;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        /// <summary>
        /// Method that gets bill with details.
        /// </summary>
        /// <param name="billId">Bill id.</param>
        /// <returns>Bill with details.</returns>
        public BillDetail getBillWithDetail(int billId)
        {
            try
            {
                dataTable.Clear();
                SqlConnection.Open();
                string query = @"Select * from BillProducts where billId=" + billId;
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                command.Fill(dataTable);
                SqlConnection.Close();

                List<ItemToPurchase> billProducts = new List<ItemToPurchase>();
                foreach (DataRow billProduct in dataTable.Rows)
                {
                    // Store Bill Products   
                    switch ((int)billProduct["productType"])
                    {
                        case (int)ProductType.BOOK: // Book
                            dataTable2.Clear();
                            SqlConnection.Open();
                            string query2 = @"Select * from Books where id=" + billProduct["productId"].ToString();
                            SqlDataAdapter command2 = new SqlDataAdapter(query2, SqlConnection);
                            command2.Fill(dataTable2);
                            SqlConnection.Close();
                            DataRow productBook = dataTable2.Rows[0];

                            Book book = (Book)ProductFactory.getInstance().ProduceProduct(ProductType.BOOK);
                            book.ID = productBook["id"].ToString();
                            book.Name = productBook["name"].ToString();
                            book.Price = float.Parse(productBook["price"].ToString());
                            book.Picture = (byte[])productBook["picture"];
                            book.ISBN = productBook["ISBN"].ToString();
                            book.Author = productBook["author"].ToString();
                            book.Publisher = productBook["publisher"].ToString();
                            book.PageNumber = (int)productBook["pageNumber"];

                            billProducts.Add(new ItemToPurchase(book, (int)billProduct["quantity"]));
                            break;
                        case (int)ProductType.MAGAZINE: // Magazine
                            dataTable2.Clear();
                            SqlConnection.Open();
                            string query3 = @"Select * from Magazines where id=" + billProduct["productId"].ToString();
                            SqlDataAdapter command3 = new SqlDataAdapter(query3, SqlConnection);
                            command3.Fill(dataTable2);
                            SqlConnection.Close();
                            DataRow productMagazine = dataTable2.Rows[0];

                            Magazine magazine = (Magazine)ProductFactory.getInstance().ProduceProduct(ProductType.MAGAZINE);
                            magazine.ID = productMagazine["id"].ToString();
                            magazine.Name = productMagazine["name"].ToString();
                            magazine.Price = float.Parse(productMagazine["price"].ToString());
                            magazine.Picture = (byte[])productMagazine["picture"];
                            magazine.Issue = productMagazine["issue"].ToString();
                            magazine.Type = (MagazineType)productMagazine["type"];

                            billProducts.Add(new ItemToPurchase(magazine, (int)billProduct["quantity"]));
                            break;
                        case (int)ProductType.MUSIC_CD: // MusicCd
                            dataTable2.Clear();
                            SqlConnection.Open();
                            string query4 = @"Select * from MusicCds where id=" + billProduct["productId"].ToString();
                            SqlDataAdapter command4 = new SqlDataAdapter(query4, SqlConnection);
                            command4.Fill(dataTable2);
                            SqlConnection.Close();
                            DataRow productMusicCd = dataTable2.Rows[0];

                            MusicCd musicCd = (MusicCd)ProductFactory.getInstance().ProduceProduct(ProductType.MUSIC_CD);
                            musicCd.ID = productMusicCd["id"].ToString();
                            musicCd.Name = productMusicCd["name"].ToString();
                            musicCd.Price = float.Parse(productMusicCd["price"].ToString());
                            musicCd.Picture = (byte[])productMusicCd["picture"];
                            musicCd.Singer = productMusicCd["singer"].ToString();
                            musicCd.Type = (MusicCdType)productMusicCd["type"];

                            billProducts.Add(new ItemToPurchase(musicCd, (int)billProduct["quantity"]));
                            break;
                        default:
                            break;
                    }
                }

                dataTable.Clear();
                SqlConnection.Open();
                string query5 = @"Select * from Bills where id=" + billId;
                SqlDataAdapter command5 = new SqlDataAdapter(query5, SqlConnection);
                command5.Fill(dataTable);
                SqlConnection.Close();

                DataRow bill = dataTable.Rows[0];
                return new BillDetail(bill["id"].ToString(), (DateTime)bill["date"], float.Parse(bill["totalPrice"].ToString()), billProducts);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        /// <summary>
        /// Method that confirm products in shopping cart and creates bill.
        /// </summary>
        public bool confirmShoppingCart()
        {
            if (loggedCustomer != null)
            {
                try
                {
                    SqlConnection.Open();
                    string query = @"Insert into Bills(customerId,date,totalPrice) values(@customerId,@date,@totalPrice) Select CAST(scope_identity() AS int)";
                    SqlCommand command = new SqlCommand(query, SqlConnection);
                    command.Parameters.AddWithValue("@customerId", loggedCustomer.ID);
                    command.Parameters.AddWithValue("@date", DateTime.Now);
                    command.Parameters.AddWithValue("@totalPrice", loggedCustomer.ShoppingCart.getTotalPrice());
                    int billId = (int)command.ExecuteScalar();
                    SqlConnection.Close();

                    foreach (ItemToPurchase item in loggedCustomer.ShoppingCart.ItemToPurchases)
                    {
                        SqlConnection.Open();
                        string query2 = @"Insert into BillProducts(billId,productType,productId,quantity) values(@billId,@productType,@productId,@quantity)";
                        SqlCommand command2 = new SqlCommand(query2, SqlConnection);
                        command2.Parameters.AddWithValue("@billId", billId);
                        command2.Parameters.AddWithValue("@productType", (int)item.Product.ProductType);
                        command2.Parameters.AddWithValue("@productId", item.Product.ID);
                        command2.Parameters.AddWithValue("@quantity", item.Quantity);
                        command2.ExecuteNonQuery();
                        SqlConnection.Close();
                    }

                    MessageBox.Show("Your bill is created and products buyed.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return false;
        }

        #endregion
    }
}
