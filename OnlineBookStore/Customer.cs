﻿/**
 * @author  : Metin Cem Demirdaş
 * @number  : 152120181015
 * @mail    : cem.dmrds@hotmail.com
 * @date    : 04/06/2021
 * @brief   : customer class and some of its methods
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{
    enum CustomerRank
    {
        User,
        Admin
    }

    /// <summary>
    /// this class holds customer's attributes and shows them
    /// </summary>
    class Customer
    {
        private string customerID;
        private string name;
        private string address;
        private string email;
        private string username;
        private string password;
        private CustomerRank rank;
        private ShoppingCart shoppingCart;

        public string ID { get { return this.customerID; } }
        public string Name { get { return this.name; } set { this.name = value; } }
        public string Address { get { return this.address; } set { this.address = value; } }
        public string Email { get { return this.email; } set { this.email = value; } }
        public string Username { get { return this.username; } set { this.username = value; } }
        public string Password { get { return this.password; } set { this.password = value; } }
        public CustomerRank Rank { get { return this.rank; } }
        public ShoppingCart ShoppingCart { get { return this.shoppingCart; } }

        /// <summary>
        /// the constructor of the Customer class(with ID parameter) 
        /// </summary>
        /// <param name="customerID">customer's ID</param>
        /// <param name="name">customer's name</param>
        /// <param name="address">customer's address</param>
        /// <param name="email">customer's email</param>
        /// <param name="username">customer's username</param>
        /// <param name="password">customer's password</param>
        /// <param name="customerRank">customer's rank</param>
        public Customer(string customerID, string name, string address, string email, string username, string password, CustomerRank customerRank)
        {
            this.customerID = customerID;
            this.name = name;
            this.address = address;
            this.email = email;
            this.username = username;
            this.password = password;
            this.rank = customerRank;
            this.shoppingCart = new ShoppingCart(customerID);
        }
        /// <summary>
        /// the constructor of the Customer class(without the ID parameter) 
        /// </summary>
        /// <param name="name">customer's name</param>
        /// <param name="address">customer's address</param>
        /// <param name="email">customer's email</param>
        /// <param name="username">customer's username</param>
        /// <param name="password">customer's password</param>
        /// <param name="customerRank">customer's rank</param>
        public Customer(string name, string address, string email, string username, string password, CustomerRank customerRank)
        {
            this.name = name;
            this.address = address;
            this.email = email;
            this.username = username;
            this.password = password;
            this.rank = customerRank;
        }

        /// <summary>
        /// prints all of the customer's information
        /// </summary>
        public void printCustomerDetails()
        {
            Console.Write("Id: " + this.customerID + " \n" + "Name: " + this.name + " \n" + "Address: " + this.address + " \n" + "Email: " + this.email + " \n" + "Username: " + this.username + " \n");
        }
        /// <summary>
        /// saves(updates) the customer 
        /// </summary>
        public void saveCustomer()
        {
            Database.getInstance().updateCustomer(this);
        }
        /// <summary>
        /// Prints all the products the customer has
        /// </summary>
        public void printCustomerPurchases()
        {
            this.shoppingCart.printProducts();
        }
    }
}
