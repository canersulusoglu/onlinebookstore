﻿/**
 * @author  : Metin Cem Demirdaş
 * @number  : 152120181015
 * @mail    : cem.dmrds@hotmail.com
 * @date    : 04/06/2021
 * @brief   : class for the inherited product "MusicCd".
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookStore
{
    enum MusicCdType
    {
        Romance,
        HardRock,
        Country
    }
    /// <summary>
    /// this class holds MusicCd and its specific attributes. 
    /// </summary>
    class MusicCd : Product
    {
        private string singer;
        private MusicCdType type;

        public string Singer { get { return this.singer; } set { this.singer = value; } }
        public MusicCdType Type { get { return this.type; } set { this.type = value; } }
        /// <summary>
        /// Constructor of the MusicCd class
        /// </summary>
        public MusicCd()
        {
            this.productType = ProductType.MUSIC_CD;
        }
        /// <summary>
        /// This determines the type of the MusicCd
        /// </summary>
        /// <returns> the MusicCd type(string) based on Enum(MusicCdType)</returns>
        public string getTypeString()
        {
            switch (this.type)
            {
                case MusicCdType.Romance:
                    return "Romance";
                case MusicCdType.HardRock:
                    return "HardRock";
                case MusicCdType.Country:
                    return "Country";
                default:
                    return "";
            }
        }
        /// <summary>
        /// Prints all of the MusicCd's information
        /// </summary>
        public override void printProperties()
        {
            Console.Write("Id: " + this.id + " \n" + "Name: " + this.name + " \n" + "Price: " + this.price.ToString() + " \n" + "Singer: " + this.singer + " \n" + "Type: " + this.getTypeString() + " \n");
        }
    }
}
