﻿/**
*  @author  : Caner Sülüşoğlu
*  @number  : 152120181022
*  @mail    : cscaner26@gmail.com
*  @date    : 30.05.2021
*  @brief   : Bill detail class.
*/
using System;
using System.Collections.Generic;

namespace OnlineBookStore
{
    /// <summary>
    /// Class that store bill data with details.
    /// </summary>
    class BillDetail : Bill
    {
        private List<ItemToPurchase> billProducts;

        /// <summary>
        /// Bill products.
        /// </summary>
        public List<ItemToPurchase> BillProducts { get { return this.billProducts; } }

        /// <summary>
        /// BillDetail class constructor.
        /// </summary>
        /// <param name="_id">Bill id.</param>
        /// <param name="_date">Bill date.</param>
        /// <param name="_totalPrice">Bill total price.</param>
        /// <param name="_billProducts">Bill products.</param>
        public BillDetail(string _id, DateTime _date, float _totalPrice, List<ItemToPurchase> _billProducts) : base(_id, _date,_totalPrice)
        {
            this.id = _id;
            this.date = _date;
            this.totalPrice = _totalPrice;
            this.billProducts = _billProducts;
        }
    }
}
