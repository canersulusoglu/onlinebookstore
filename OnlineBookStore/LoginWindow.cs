﻿/**
*  @author  : Tarık Kemal Gündoğdu
*  @number  : 152120191054
*  @mail    : tarikkemal314@gmail.com
*  @date    : 3.06.2021
*  @brief   : Log In Window.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineBookStore
{
    /// <summary>
    /// Log In Window form page.
    /// </summary>
    public partial class LoginWindow : Form
    {
        Database db = Database.getInstance();
        Timer timer = new Timer();
        int timeLeft = 1;

        /// <summary>
        /// Log In Window constructor.
        /// </summary>
        public LoginWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that logs in the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logInBtn_Click(object sender, EventArgs e)
        {
            string userName = userNameText.Text;
            string password = passwordText.Text;

            if (userNameText.TextLength < 1)
            {
                successLabel.ForeColor = Color.Red;
                successLabel.Text = "Please enter your email address.";
                return;
            }
            else if (passwordText.TextLength < 1)
            {
                successLabel.ForeColor = Color.Red;
                successLabel.Text = "Please enter your password.";
                return;
            }

            bool userExists = db.Login(userName, password);

            if (userExists)
            {
                successLabel.Text = "Logging In...";
                successLabel.ForeColor = Color.Green;

                timer.Interval = 1000;
                timer.Enabled = true;

                timer.Tick += timer_Tick;
            }
            else
            {
                successLabel.Text = "Your email or password is wrong.";
                successLabel.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Method that counts down the timer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            timeLeft -= 1;

            if (timeLeft == 0)
            {
                this.Hide();
                windowHome home = new windowHome();
                home.FormClosed += (s, args) =>
                {
                    this.Close();
                };
                home.Show();
                timer.Stop();
            }
        }

        /// <summary>
        /// Method that takes the user to Sing Up window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void goToSignUpLabel_Click(object sender, EventArgs e)
        {
            this.Hide();
            SignUpWindow signUpWindow = new SignUpWindow();
            signUpWindow.FormClosed += (s, args) =>
            {
                this.Close();
            };
            signUpWindow.Show();
        }
    }
}
